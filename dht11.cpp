#include "dht11.h"

SignalDuration dht11::checkSignalDuration(uint8_t pin, uint8_t mode) {
    // 10000 iters is upper than 100 micro seconds of duration
    // dht communication signals are never upper this duration
    // if signal is upper to this duration, return a timeout
    int await_loop_idx = 10000;
    while (digitalRead(pin) == mode)
        if (await_loop_idx-- == 0)
            return SignalDuration::TIMEOUT;
    return SignalDuration::OK;
}

/// read results from Dht11 sensors
DhtReadResult dht11::read(int8_t pin) {
    uint8_t data[5] = {0};

    // sending start signal (at least 16 ms of low voltage),
    // pulling-up signal at least 40 us for response signal
    // between these step, dht11 changes to low-power mode to running mode,
    // waiting to MCU to completing the starting signals
    pinMode(pin, OUTPUT);
	digitalWrite(pin, LOW);
	delay(18);
	digitalWrite(pin, HIGH);
	delayMicroseconds(40);
	pinMode(pin, INPUT);

    // await for dht11 response signal.
    // dht11 response signal is performed by a low voltage followed by a high
    // voltage, each of them during approx 80 us.
    if (checkSignalDuration(pin, LOW) == SignalDuration::TIMEOUT)
        return DhtReadResult::ERROR_TIMEOUT;
    
    if (checkSignalDuration(pin, HIGH) == SignalDuration::TIMEOUT)
        return DhtReadResult::ERROR_TIMEOUT;

    // reading data sent by dht11
    // each bit received started by a low-voltage during 50 us,
    // then bit's value is represented by a high-voltage and its value depends
    // on duration of the signal (26-28 us -> 0 | 70us -> 1)
    uint8_t cur_bit = 7;
    uint8_t cur_byte = 0;

    for (uint8_t i = 0; i < 40; i++) {    

        if (checkSignalDuration(pin, LOW) == SignalDuration::TIMEOUT)
        return DhtReadResult::ERROR_TIMEOUT;

        // record high-voltage duration to determine if bit is on or off
        uint64_t  voltage_length_begin = micros();
        if (checkSignalDuration(pin, HIGH) == SignalDuration::TIMEOUT)
        return DhtReadResult::ERROR_TIMEOUT;

        // check bit value and assign it
        if (micros() - voltage_length_begin > 40)
            data[cur_byte] |= (1 << cur_bit);
        if (cur_bit == 0) {
            cur_bit = 7;
            cur_byte += 1;
        }
        else cur_bit -= 1;
    }

    // compare sum of first 4 bytes with parity byte (last)
    if (data[0] + data[1] + data[2] + data[3] != data[4]) {
        return DhtReadResult::ERROR_CHECKSUM;
    }

    dht11::humidity = data[0];
    dht11::temperature = data[2] + float(data[3]) / 10;

    return DhtReadResult::OK;
}
