#ifndef DHT11_H
# define DHT11_H

#if defined(ARDUINO) && (ARDUINO >= 100)
# include <Arduino.h>
#else
    #error "arduino lib is not found"
#endif

enum class DhtReadResult {
    OK,
    ERROR_CHECKSUM,
    ERROR_TIMEOUT,
};

enum class SignalDuration {
    OK,
    TIMEOUT,
};

// represents an instance of DHT module
class dht11 {
    public:
        DhtReadResult read(int8_t pin);
	    int     humidity;
	    float   temperature;
    private:
        SignalDuration checkSignalDuration(uint8_t pin, uint8_t mode);
};

#endif
